let chaine = "ma formation javascript";
console.log(chaine);

// – Retourner la position de « ma »
console.log(chaine.indexOf("ma"));
console.log(chaine.lastIndexOf("ma"))
console.log(chaine.search("ma"));

// – Indiquer l’indice de la lettre « p »
console.log(chaine.indexOf("p"));
console.log(chaine.lastIndexOf("p"))
console.log(chaine.search("p"));

// – Retrouver la lettre située à l’indice 21
console.log(chaine[21]);
console.log(chaine.charAt(21));
console.log(chaine.at(21));
console.log(chaine.at(-2)); //Pour partir de la fin

// – Remplacer « javascript » par « Java »
// ! attention, ne modifie pas la chaine, si on veut save le changement, il faudra remplacer chaine par elle même modifiée
console.log(chaine.replace("javascript", "Java"))
console.log(chaine);

// – Découper la chaîne avec le délimiteur «  » (espace)
console.log(chaine.split(" "));

// – Inverser la chaîne de caractères (+ difficile) :
// « ma formation javascript » → « tpircsavaj noitamrof am »

//Comme on ne peut pas reverse une chaine directement on va :


let tab = chaine.split(""); //transformer la chaine en un tableau qui contient chacun des caractères dans les cases
let tabReverse = tab.reverse(); //inverser ce tableau
let stringReverse = tabReverse.join(""); //rejoindre toutes les lettres pour reformer une chaine

console.log(chaine.split("").reverse().join(""));

console.log(stringReverse);

//Si on voulait inverser juste un mot :
let tab1 = chaine.split(" ");
tab1[1] = tab1[1].split("").reverse().join("");
console.log(tab1.join(" "));