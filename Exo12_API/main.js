const INPUT_NAME = document.getElementById("name")
const SELECT_COUNTRY = document.getElementById("country")
const BTN_GENDERIZE = document.getElementById("genderize")
const DIV_RES = document.getElementById("resultat")


BTN_GENDERIZE.addEventListener("click", async () => {
    BARRE_FRONT.textContent = "";
    BARRE_BACK.textContent = "";

    //?1) Vérifier que nos champs sont bien remplis
    let name = INPUT_NAME.value;
    let countryCode = SELECT_COUNTRY.value;
    if(name !== '' && countryCode !== ''){

        //?2) We do the requête
        let res = await axios.get(`https://api.genderize.io/?name=${name}&country_id=${countryCode}`)

        DIV_RES.textContent = `${name} a ${res.data.probability * 100}% de chance d'être ${ res.data.gender == "male" ? "un homme" : "une femme" } dans le pays dont le code est ${res.data.country_id}`

console.log(res.data.probability);
        

        if(res.data.gender == "female") {
            remplirJauge(res.data.probability*100);
            BARRE_FRONT.textContent = res.data.probability *100 + "%";
            BARRE_BACK.textContent = 100 - res.data.probability * 100 + "%";
        }
        else {
            remplirJauge(100 - res.data.probability*100);
            BARRE_BACK.textContent = res.data.probability *100 + "%";
            BARRE_FRONT.textContent = 100 - res.data.probability * 100 + "%";
        }

    }
    
})

const BARRE_FRONT = document.querySelector(".front")
const BARRE_BACK = document.querySelector(".back p")

function remplirJauge(pourcentage) {
    console.log("LESGOOO");
    let remplissage = 0;
    let timer = setInterval(() => {
        remplissage += 2;
        if(remplissage >= pourcentage) {
            BARRE_FRONT.style.width = pourcentage + '%';
            clearInterval(timer);
        }
        else {
            BARRE_FRONT.style.width = remplissage + '%';
        }
    }, 100)
}