// Exo 1 : Créez un formulaire simple : Nom, Prénom.
// À l’aide du JavaScript, proposez une complétion automatique du formulaire (Jean, 
// Dupont) lorsque l’on appuie sur un bouton.
// Ajoutez un bouton qui remet à zéro tous les champs du formulaire
// ----------------------------------------------------------------------
// Reprenez le formulaire précédent. Ajoutez-y un champ Code Postal.
// À l’aide du JavaScript, faites les vérifications nécessaires sur les champs.
// → Un code postal est un nombre de 4 chiffres (entre 1000 et 9999).
// Vérifiez si les champs sont bien remplis, et si le code postal est conforme.
// Lorsque le formulaire est valide, un message s’affiche sur la page.

// Déclaration de notre regex
const LETTER_ONLY = /^[\D]*$/

// Récupération des éléments
const FORMULAIRE = document.exo1;
const BTN_REMPLIR = document.getElementById("fill");
const BTN_VIDER = document.getElementById("empty");
const BTN_ENVOYER = document.getElementById("send");
const DIV_MSG = document.getElementById("message");

BTN_REMPLIR.addEventListener("click", () => {
    FORMULAIRE.lastname.value = "Dupont";
    FORMULAIRE.firstname.value = "Jean";
})

BTN_VIDER.addEventListener('click', () => {
    FORMULAIRE.reset();
})


FORMULAIRE.zipcode.addEventListener("input", checkValidity);
FORMULAIRE.lastname.addEventListener("input", checkValidity);
FORMULAIRE.firstname.addEventListener("input", checkValidity);


function checkValidity() {
    // Est-ce que l'input code postal est valide ?
    // Est-ce que l'input nom est valide ?
    // Est-ce que l'input prénom est valide ?
    const formValid = checkLastName() && checkFirstName() && checkZipCode(); 

    // Si tout est valide -> le boutton soumettre est clickable
    // Si tout n'est pas valide -> le boutton soumettre n'est pas clickable
    BTN_ENVOYER.disabled = !formValid;
    DIV_MSG.innerText = (formValid) ? "Formulaire Valide" : "Formulaire invalide";
}

function checkLastName() {
    const lastname = FORMULAIRE.lastname.value;
    if(lastname != "" && lastname.length < 50 && LETTER_ONLY.test(lastname)) {
        return true;
    }
    else {
        return false;
    }
}

function checkFirstName() {
    const firstname = FORMULAIRE.firstname.value;
    if(firstname != "" && firstname.length < 50 && LETTER_ONLY.test(firstname)) {
        return true;
    }
    else {
        return false;
    }
}

function checkZipCode() {
    const zipCode = FORMULAIRE.zipcode.value;
    if(zipCode != "" && ( zipCode >= 1000 && zipCode <= 9999 )) {
        return true;
    }
    else {
        return false;
    }
}