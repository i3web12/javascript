// Reprenez le formulaire précédent. Ajoutez-y un champ Code Postal.
// À l’aide du JavaScript, faites les vérifications nécessaires sur les champs.
// → Un code postal est un nombre de 4 chiffres (entre 1000 et 9999).
// Vérifiez si les champs sont bien remplis, et si le code postal est conforme.
// Lorsque le formulaire est valide, un message s’affiche sur la page.

