const IMAGES = [
    "https://images.pexels.com/photos/1382393/pexels-photo-1382393.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",

    "https://images.pexels.com/photos/1477166/pexels-photo-1477166.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",

    "https://images.pexels.com/photos/5712883/pexels-photo-5712883.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",

    "https://images.pexels.com/photos/1666529/pexels-photo-1666529.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",

    "https://images.pexels.com/photos/10647646/pexels-photo-10647646.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",

    "https://images.pexels.com/photos/1073078/pexels-photo-1073078.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
]
const ALTS = [
    "Caladium, Oreilles d'éléphant",
    "Marguerite du Cap, Marguerite d'Afrique",
    "Syngonium podophyllum, Patte-d'oie",
    "Épine du Christ, Euphorbes",
    "Monstera deliciosa",
    "Lys plantain de Siebold, Hosta"
]

// Déclaration élements HTML :
const IMG_CAROUSEL = document.querySelector('.carousel img')
const DOTS = document.getElementById('dots');
const PREV = document.getElementById('prev');
const NEXT = document.getElementById('next');
const CAROUSEL = document.querySelector('.carousel');

// Déclaration variables pour la logique du code :
let currentImg = 0; //Indice actuel dans les tableaux
let timer;


// Setup de la première image dans le HTML
IMG_CAROUSEL.src = IMAGES[currentImg];
IMG_CAROUSEL.alt = ALTS[currentImg];

// Fabriquer les points
IMAGES.forEach(image => {
    DOTS.innerHTML += " <div class='dot'> </div> "
});
// for(let i = 0; i < IMAGES.length; i++) {
//     DOTS.innerHTML += " <div class='dot'> </div> "
// } //Equivalent foreach

// Setup pour mettre le premier point en actif
DOTS.children[currentImg].classList.add('active-dot')

// Ajout des events sur les flèches gauche et droite
PREV.addEventListener('click', () => {
    //Après opti avec fonction
    changeImage(-1);

    // #region Avant opti
    // DOTS.children[currentImg].classList.remove('active-dot');

    // currentImg = currentImg - 1;
    // // On vérifie si on est sorti du tableau
    // if(currentImg < 0) {
    //     currentImg = IMAGES.length - 1;
    // }

    // // Une fois qu'on a changé l'indice courant,
    // // on peut changer la source de l'img, le alt et le dot
    // IMG_CAROUSEL.src = IMAGES[currentImg];
    // IMG_CAROUSEL.alt = ALTS[currentImg];
    // DOTS.children[currentImg].classList.add('active-dot');
    //#endregion
})

NEXT.addEventListener('click', () => {
    // Après opti
    changeImage(1);

    //#region Avant opti
    // DOTS.children[currentImg].classList.remove('active-dot');

    // currentImg = currentImg + 1;
    // if(currentImg >= IMAGES.length) {
    //     currentImg = 0;
    // }

    // IMG_CAROUSEL.src = IMAGES[currentImg];
    // IMG_CAROUSEL.alt = ALTS[currentImg];
    // DOTS.children[currentImg].classList.add('active-dot');
    //#endregion Avant opti

})

// Ajout du timer
timer = setInterval(() => {
    changeImage(1);
}, 1750)

// Ajout de l'event pour stopper le timer quand on a la souris sur le carousel
CAROUSEL.addEventListener('mouseover', () => {
    clearInterval(timer);
})

CAROUSEL.addEventListener('mouseleave', () => {
    timer = setInterval(() => {
        changeImage(1);
    }, 1750)
})

// Changer les images avec les dots
//DOTS.children étant une list d'éléments HTML et non un tableau Array.from() va permettre de le transformer en tableau pour pouvoir faire une boucle dessus
Array.from(DOTS.children).forEach((DOT, indice) => {
    DOT.addEventListener('click', () => {
        DOTS.children[currentImg].classList.remove('active-dot');

        currentImg = indice;

        IMG_CAROUSEL.src = IMAGES[currentImg];
        IMG_CAROUSEL.alt = ALTS[currentImg];
        DOTS.children[currentImg].classList.add('active-dot');
    })
})


// Optimisation -> fonctions
function changeImage(count) {
    DOTS.children[currentImg].classList.remove('active-dot');

    // currentImg = currentImg + count;
    currentImg += count;
    //#region explication
    // Si count = 1
    // currentImg = currentImg + 1;
    // Si count = -1
    // currentImg = currentImg + -1;
    // donc currentImg = currentImg - 1;
    //#endregion explication

    if(currentImg < 0) {
        currentImg = IMAGES.length - 1;
    }
    if(currentImg >= IMAGES.length) {
        currentImg = 0;
    }

    //#region Pareil avec la ternaire
    // currentImg = ( currentImg < 0 ) ? IMAGES.length - 1 : ( currentImg >= IMAGES.length ) ? 0 : currentImg;
    //#endregion

    IMG_CAROUSEL.src = IMAGES[currentImg];
    IMG_CAROUSEL.alt = ALTS[currentImg];
    DOTS.children[currentImg].classList.add('active-dot');
}

