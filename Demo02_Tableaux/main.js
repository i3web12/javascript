// ? Déclarer un tableau :
// ! La plus utilisée ↓
let monTableauVide = [];
let monTableauRempli = ["Pierre", "Paul", "Jacques"]; 

// Moins utilisé
let monTableauVide2 = new Array();
let monTableauDe10Cases = new Array(10);
let monTableauRempli2 = new Array("Pierre", "Paul", "Jacques");

// Pour accéder à une valeur en particulier
// Opérateur d'accès : []
console.log( monTableauRempli[1] ); //Affiche Paul
// Pour modifier une valeur 
monTableauRempli[2] = "Jake"; //Modification de la valeur à l'indice 2
console.log(monTableauRempli);
// Si vous modifiez à un indice pas présent dans le tableau
monTableauRempli[12] = "Pouet";
console.log(monTableauRempli);
// Attention, créera des cases vides entre la 2 et la 12

// ???? Quelques méthodes utiles sur les tableaux ????
let tableauBase = ["Khun"];
console.clear();
console.log("Avant Ajouts");
console.log(tableauBase);
console.log("Taille tableau : " + tableauBase.length);

// Pour ajouter une valeur dans un tableau
// ! A la fin (plus utilisé)
console.log("Push !");
tableauBase.push("Aurélien"); //1 seule valeur
tableauBase.push("Aude", "Alexandre"); // ou plusieurs valeurs
console.log(tableauBase);
console.log("Taille tableau : " + tableauBase.length);
// Au début (moins utilisé)
console.log("Unshift !");
tableauBase.unshift("Quentin");
console.log(tableauBase);

// Pour enlever une valeur
// Le copain du push -> enlève toujours le dernier élément
console.log("Pop !");
tableauBase.pop()
console.log(tableauBase);
// Le copain du unshift -> enlève toujours le premier élément
console.log("Shift !");
tableauBase.shift();
console.log(tableauBase);

//La plupart du temps, on ne veut enlever et/ou récupérer ni le dernier, ni le premier mais UN (ou plusieurs) en particulier
tableauBase.push("Mélanie");
console.log(tableauBase);

// SLICE -> Vous permet de récupérer un bout du tableau, sans modifier pour antant le tableau
let formateursHTML = tableauBase.slice(1, 3) //Récupère les éléments de l'indice 1 à l'indice 3 (non compris)
console.log(formateursHTML);
console.log(tableauBase);

// SPLICE -> Vous permet de récupérer un bout du tableau, le tableau est modifié, la partie récupérée d'existe plus dedans
let formateursHTML2 = tableauBase.splice(1, 2) //Découpe à partir de l'indice 1 et prend 2 éléments
console.log(formateursHTML2);
console.log(tableauBase);

// Pour afficher un tableau sous forme de chaine :
console.log(tableauBase);
console.log(tableauBase.toString()); //Concatène toutes les valeurs du tableau séparées par ,
console.log(tableauBase.join(' ')); // Fait comme le toString() mais vous pouvez choisir ce qui sépare chacune des valeurs
let tab1 = ["Lundi", "Mardi"];
let tab2 = ["Mercredi", "Jeudi"];
let tab3 = tab1 + tab2; //Instinctivement on aimerait concaténer nos deux tableaux comme ça mais non
// ! Utiliser concat
let tab4 = tab1.concat(tab2).concat("Vendredi");
let tab5 = [...tab1, ...tab2, "Vendredi"]; //... spread operator : En gros, enlève les [] autour du tableau, on ne récupère que les valeurs dedans, on l'a "spread"
console.log(tab3);
console.log(tab4);
console.log(tab5);
// console.log(tab1); // console.log(tab2); // Pas modifiés

console.log( Math.ceil(Math.random() * 10));

// Reverse
let tab = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
tab.reverse();
console.log(tab);

// Sort
let aTrierToutVaBienSePasser = ["Quentin", "Aude", "Khun", "Sam", "Aurélien"];
aTrierToutVaBienSePasser.sort();
console.log(aTrierToutVaBienSePasser);

let aTrierToutVaPasBienSePasser = ["Quentin", "Aude",  "Äude", "khun", "Sam", "Aurélien", "Aurelien"]
aTrierToutVaPasBienSePasser.sort((a, b) => a.localeCompare(b));
console.log(aTrierToutVaPasBienSePasser);

let nombres = [1, 56, 23, 5, 147, 502, "khun", "Khun"];
nombres.sort((a, b) =>  a - b) //Croissant
console.log(nombres);
nombres.sort((a, b) =>  b - a); //Décroissant
console.log(nombres);
