// Écrivez un programme qui permet à l'utilisateur de convertir une température 
// entre Celsius (°C) et Fahrenheit (°F). L'utilisateur doit saisir la température et 
// l'unité de mesure (C pour Celsius, F pour Fahrenheit). Utilisez une déclaration 
// "switch" pour gérer les deux unités de mesure et effectuer la conversion. 
// Affichez la température convertie.

const temperature = parseFloat(prompt("Entrez la température"))

const unite = prompt("Entrez l'unité de mesure (C pour celsius, F pour Fahrenheit)").toUpperCase() // Permet de convertir en majuscule

let temperatureConvertie;

switch (unite) {
    case "C":
        temperatureConvertie = (temperature * 9/5) + 32;
        // console.log("La température en Fahrenheit est de : " + temperatureConvertie + " °F")
        console.log(`La température en Fahrenheit est de : ${temperatureConvertie} °F`)
        break;
    case "F":
        temperatureConvertie = (temperature - 32) * 5/9;
        console.log(`La température en Celsius est de : ${temperatureConvertie} °C`)
        break;
    default:
        console.log("Unite de mesure non valide, Utilisez C pour Celsius ou F pour Fahrenheit.")
        break;
}