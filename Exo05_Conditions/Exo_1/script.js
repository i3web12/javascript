// 1. Écrivez un programme de calculatrice simple qui demande 
// à l'utilisateur deux nombres et un opérateur (+, -, *, /) 
// pour effectuer une opération, en utilisant une déclaration switch.


// déclaration + initialisation de mes variables
let nombre1 = parseFloat(prompt("Entrer le premier nombre :"))
let operateur = prompt("Entrez l'opérateur (+, -, *, /) :")
let nombre2 = parseFloat(prompt("Entrer le deuxième nombre :"))

switch (operateur) {
    case "+":
        console.log(`${nombre1} + ${nombre2} = ${(nombre1+nombre2)}`)
        break;
    case "-":
        console.log("Resultat :", (nombre1-nombre2))
        break;
    case "*":
        console.log("Resultat :", (nombre1*nombre2))
        break;
    case "/":
        if (nombre2 !== 0) {
            console.log("Resultat :", (nombre1/nombre2))
        }
        else{
            console.log("Division pas réro impossible !")
        }
        break;
    default:
        console.log("Opérateur invalide...")
        break;
}