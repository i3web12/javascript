// // Écrivez un programme qui permet à l'utilisateur de saisir sa date de naissance 
// // (jour, mois, année) et calcule son âge en années, mois et jours. Utilisez des 
// // déclarations "if" pour gérer les années bissextiles et une déclaration "switch" 
// // pour gérer les mois. Affichez l'âge calculé.

// // Récupération des informations utilisateur
// let jourNaissance = parseInt(prompt("Entrez  le jour de votre naissance :"));
// let moisNaissance = parseInt(prompt("Entrez  le mois de votre naissance (1-12) :"));
// let anneeNaissance = parseInt(prompt("Entrez  l'année de votre naissance :"));

// // Obtention de la date actuelle
// let dateActuelle = new Date();
// let anneeActuelle = dateActuelle.getFullYear();
// let moisActuel = dateActuelle.getMonth() +1; // Les mois commencent à partir de 0
// let jourActuel = dateActuelle.getDate();

// // calculez l'age en années en tenant compte de la date actuelle et de la date de naissance
// let age = anneeActuelle - anneeNaissance;

// // verifier si l'anniversaire est déjà passé cette année 
// if (moisActuel < moisNaissance || (moisActuel === moisNaissance && jourActuel < jourNaissance)) {
//     age--;
// }

// // Gérez les année bissextiles en déterminant le nombre de jours dans le mois de février => Condition Classique
//     // let jourDansFevrier;
//     // if (((anneeNaissance % 4 === 0) && (anneeNaissance % 100 !== 0)) || (anneeNaissance % 400 === 0)) {
//     //     jourDansFevrier = 29;
//     // }
//     // else{
//     //     jourDansFevrier = 28;
//     // }

// // Gérez les année bissextiles en déterminant le nombre de jours dans le mois de février => Condition ternaire
// let jourDansFevrier = (((anneeNaissance % 4 === 0) && (anneeNaissance % 100 !== 0)) || (anneeNaissance % 400 === 0)) ? 29 : 28

// let mois

// switch (moisNaissance) {
//     case 1:
//         mois = "Janvier";
//         break;
//     case 2:
//         mois = "Fevrier";
//         break;
//     case 3:
//         mois = "Mars";
//         break;
//     case 4:
//         mois = "Avril";
//         break;
//     case 5:
//         mois = "Mai";
//         break;
//     case 6:
//         mois = "Juin";
//         break;
//     case 7:
//         mois = "Juillet";
//         break;
//     case 8:
//         mois = "Aout";
//         break;
//     case 9:
//         mois = "Septembre";
//         break;
//     case 10:
//         mois = "Octobre";
//         break;
//     case 11:
//         mois = "Novembre";
//         break;
//     case 12:
//         mois = "Decembre";
//         break;
//     default:
//         mois = "Mois inconnu";
//         break;
// }

// // Affichez la date de naissance en format texte et l'àge calculé
// console.log(`Vous êtes né(e) le ${jourNaissance} ${mois} ${anneeNaissance}.`)
// console.log(`Vous avez ${age} ans.`)

// V_2 => Sanae

//date de naissance
let JourNaissance = parseInt(prompt("Veuillez entrer votre jour de naissance"));
let MoisNaissance = parseInt(prompt("veuillez entrer votre mois de naissance"));
let AnneeNaissance = parseInt(prompt("Veuillez entrer votre année de naissance"));

//date actuelle
let DateActuelle = new Date();
console.log(DateActuelle.getFullYear());
console.log(DateActuelle.getMonth() + 1);
console.log(DateActuelle.getDate());

//age
let AnneeAge = DateActuelle.getFullYear() - AnneeNaissance;
let MoisAge = (DateActuelle.getMonth() + 1) - MoisNaissance;
let JourAge = DateActuelle.getDate() - JourNaissance;

prompt(`Vous avez ${AnneeAge} an(s), ${MoisAge} mois et ${JourAge} jour(s)`);



// Calcul de la date du prochain anniversaire pour cette année
let prochainAnniversaire = new Date(DateActuelle.getFullYear(), MoisNaissance, JourNaissance);

// Si la date actuelle est après la date du prochain anniversaire, on ajoute 1 an
if (DateActuelle > prochainAnniversaire) {
  prochainAnniversaire.setFullYear(prochainAnniversaire.getFullYear() + 1); // setFullYeardéfinit l'année complête pour une date
}


// Calcul les jours restants
let unJourEnMillisecondes = 24 * 60 * 60 * 1000; // Nombre de millisecondes dans une journée
let joursRestants = Math.ceil((prochainAnniversaire - DateActuelle) / unJourEnMillisecondes); //Math.ceil() retourne le plus petit entier supérieur ou égal au nombre donné.

// Affichage du résultat en ternaire

alert(`Il reste ${joursRestants} jour${joursRestants > 1 ? 's' : (joursRestants === 1 ? '' : 's')} avant votre prochain anniversaire`)