// // Écrivez un programme pour gérer une liste de rendez-vous. L'utilisateur peut 
// // ajouter de nouveaux rendez-vous, supprimer des rendez-vous existants ou 
// // afficher tous les rendez-vous. Utilisez une déclaration "switch" pour gérer les 
// // options du menu principal (Ajouter, Supprimer, Afficher) et des déclarations 
// // "if" pour gérer chaque action spécifique.

// const rdv = ["Denstiste","Dermatologue","Vétérinaire"]
// console.log("-------------------------------")
// console.log("\n Menu de gestion de rendez-vous :");
// console.log("1. Ajouter un rendez-vous");
// console.log("2. Supprimer un rendez-vous");
// console.log("3. Afficher tous les rendez-vous");
// console.log("4. Quitter");
// console.log("-------------------------------")


// let choixUser = parseInt(prompt("Choisissez une option (1/2/3/4) : "));
// let nouveauRdv

// switch (choixUser) {
//     case 1:
//         nouveauRdv = prompt("Entre le nouveau rdv");
//         rdv.push(nouveauRdv)
//         console.log("Rendez-vous ajouté avec succès !")
//         break;
//     case 2:
//         const indexASupprimer = parseInt(prompt("Entrez l'indice du rendez-vous à supprimer :"))
//         if (indexASupprimer >= 1 && indexASupprimer <= rdv.length) {
//             rdv.splice(indexASupprimer-1, 1)
//             console.log("Rendez-vous supprimé avec succès !")
//         }
//         else{
//             console.log("Indice non valide.")
//         }
//         break;
//     case 3:
//         console.log("\n Liste des rendez-vous")
//         console.log(`- ${rdv[0]}`)
//         console.log(`- ${rdv[1]}`)
//         console.log(`- ${rdv[2]}`)
//         console.log(`- ${rdv[3]}`)
//         console.log(`- ${rdv[4]}`)
//         break;
//     case 4:
//         console.log("Au revoir !")
//         break;
// }

// console.log(rdv)


let JourNaissance = parseInt(prompt("Veuillez entrer votre jour de naissance"));
let MoisNaissance = parseInt(prompt("veuillez entrer votre mois de naissance"));
let AnneeNaissance = parseInt(prompt("Veuillez entrer votre année de naissance"));

let DateActuelle = new Date();
console.log(DateActuelle.getFullYear());
console.log(DateActuelle.getMonth() + 1);
console.log(DateActuelle.getDate());

let AnneeAge = DateActuelle.getFullYear() - AnneeNaissance;
let MoisAge = (DateActuelle.getMonth() + 1) - MoisNaissance;
let JourAge = DateActuelle.getDate() - JourNaissance;

console.log(`Vous avez ${AnneeAge} an(s), ${MoisAge} mois et ${JourAge} jour(s)`);