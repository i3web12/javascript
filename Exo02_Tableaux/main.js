// Le programme demande trois fois à l'utilisateur d'entrer un de ces hobbies et les stock dans un tableau
let hobbies = [];
// let hobbies = [prompt(), prompt(), prompt()]
// hobbies.push(prompt(), prompt() , prompt())
// hobbies.push(prompt("Entrez votre hobby N°1"));
// hobbies.push(prompt("Entrez votre hobby N°2"));
// hobbies.push(prompt("Entrez votre hobby N°3"));

// Avant goût de ce qu'on fera après les vacances (et on verra encore deux autres boucles for 😁)
for(let i = 1; i < 4; i++ ) {
    hobbies.push(prompt(`Entrez votre hobby N°${i}`));
}

// Ensuite, on trie ces hobbies pour qu'ils soient classés dans l'ordre alphabétique
hobbies.sort((a, b) => a.localeCompare(b));
//Depuis 2015 ↑ remplace ↓ (écriture raccourcie)
//hobbies.sort(function(a, b) { return a.localeCompare(b)})

// Et on affiche "Vos hobbies sont Hobby1 - Hobby2 - Hobby3"
//alert(`Vos hobbies sont ${hobbies}`)
// Automatiquement comme il peut pas afficher le tableau en soit, la méthode toString() a été appliquée
alert(`Vos hobbies sont ${hobbies.join(" - ")}`)
 
// Bonus : Demandez ensuite à l'utilisateur quel hobby il souhaite supprimer de la liste (1, 2 ou 3),
// let hobbyNumber = parseInt(prompt(`Quel élément voulez vous supprimer ?
// 1-${hobbies[0]}
// 2-${hobbies[1]}
// 3-${hobbies[2]} `))

//Bonus ++ : Permettre de taper le hobby
let hobbyASupprimer = prompt("Quel hobby voulez-vous supprimer ?");
let hobbyNumber = hobbies.findIndex( h => h == hobbyASupprimer )

// le supprimer 
// hobbies.splice(hobbyNumber-1 , 1);
if(hobbyNumber != -1){
    hobbies.splice(hobbyNumber , 1);
    // et réafficher "Vos hobbies sont maintenant HobbyX - HobbyX"
    alert(`Vos hobbies sont maintenant ${hobbies.join(" - ")}`)
}
else {
    alert("Ce hobby n'est pas dans la liste !")
}

