console.log('Coucou depuis le fichier js')

// Récupérer la div demo-defer
const DIV_DEMO_DEFER = document.getElementById('demo-defer');
// Modifier le texte pour afficher Bonjour à la place de Coucou
DIV_DEMO_DEFER.innerHTML = 'Bonjour';

// Rappel commentaires
/* Commentaires
Sur
Plusieurs
Lignes */
// Raccourcis : Ctrl + :
// Extension : Better Comments
// ! Important
// ? Question
// * Surbrillance
// TODO Truc à faire


// Afficher une pop-up
//alert('Coucou');
// Récupérer une info
//let userTruc = prompt('Entrez un truc :')

// * -------- CONSTANTES ET VARIABLES --------
// * Les constantes prédéfinies
// * NaN -> On va pas mal le rencontrer puisqu'on va l'obtenir soit en réponse d'une opération mathématique pas ok, soit après une conversion impossible
console.log("15 - 'bidule' = " , 15 - 'bidule');
console.log("parseInt('pouet') = ", parseInt('pouet'));
// Pour vérifier si quelque chose est NaN
// ? Est-ce que (15 - 'bidule') est égal à NaN ?
console.log("Sans isNaN : ", (15 - 'bidule') === NaN ); //false
// ! Pour pouvoir tester si quelque chose est NaN
console.log("Avec isNaN : ", isNaN( 15 - 'bidule') );

// * undefined : Rencontré quand notre variable est déclarée, elle existe mais elle est vide, il n'y a aucune valeur à l'intérieur
// Déclaration d'une variable
let pouet;
// Initialisation : Première affectation de valeur dans notre variable
pouet = 12;
// Réaffection de variable : Je change la valeur
pouet = 42;

let truc;
console.log(truc);
//truc.toUpperCase();

// * Convention de nommage des variables et constantes
let nomVariable;
// 1) lowerCamelCase (Première lettre en minuscule et pour chaque mot dans la variable, majuscule) 
// 2) Pas d'accent, pas de caractères spéciaux (sauf $ et _ mais ils ont des significations précises, dont on évite de les utiliser pour rien) 
// 3) Les chiffres sont autorisés mais pas en PREMIER caractère
//! 4) Privilégier les noms en anglais dans la mesure du possible et surtout, choisir des noms parlants (on évite les trucs, bidule, test, caca, les lettres (sauf cas particuliers))

const MA_SUPER_CONSTANTE = 0;
// 1) Même règles qu'au dessus sauf pour la 1, on utilise cette fois le UPPERCASE (UPPER_SNAKE_CASE) et on met un _ entre deux mots

// * Nos propres constantes
// le mot clef : const
// Attention, il faudra l'initialiser en même temps qu'on la déclare
const FAV_COLOR = 'Green';
// ! Attention la réassignation dans VSC ne se soulignera pas pour vour avertir qu'il y a une erreur mais ceci provoquera bien une erreur dans la console
// FAV_COLOR = 'Red';
// ? On verra plus tard que les constantes fonctionnent un peu différemment avec les tableaux et les objets

// * Les types de base : types littéraux
// Le type Number : En Js, tout nombre est de type Number, pas de distinction entre int, float
let monEntier = 15; 
let monReel = 42.5;
// Le type string : En Js, pas de distinction entre chaine et caractère, tout est string
// Vous choisissez entre "" et '', c'est pareil, soyez juste constantes
let maChaine = "Aujourd'hui : \"Ma\" chaine";
let maChaine2 = 'Aujourd\'hui : Ma chaine';
let monChar = "j";
// Les tableaux
let monTableau = [1, 2, 3, 4];
// Les tableaux associatifs (aussi appelés objets)
let monTabeauAsso = {
    clef1 : "valeur1",
    clef2 : "valeur2"
}
let maListDAnniv = {
    "Aurélien" : new Date(1989, 10, 1),
    "Aude" : new Date(1989, 9, 16),
    6 : "Rouflaquette",
    true : 1
}
let person = {
    nom : 'Beurive',
    prenom : 'Aude'
}
console.clear();
console.log(maListDAnniv["Aurélien"]);
console.log(person.prenom);

//* --- Différence entre var et let ---

var varX = 5
function varFonction() {
    var varY = 7;
    if(true) {
        var varZ = 9;
        console.log("varX if : ", varX); //5
        console.log("varY if : ", varY); //7
        console.log("varZ if : ", varZ); //9
    }

    console.log("varX fc : ", varX); //5
    console.log("varY fc : ", varY); //7
    console.log("varZ fc : ", varZ); //9
}

varFonction();
console.log("varX dehors : ", varX); //5
// console.log("varY dehors : ", varY); //NOPE
// console.log("varZ dehors : ", varZ); //NOPE

let letX = 5
function letFonction() {
    let letY = 7;
    if(true) {
        let letZ = 9;
        console.log("letX if : ", letX); //5
        console.log("letY if : ", letY); //7
        console.log("letZ if : ", letZ); //9
    }

    console.log("letX fc : ", letX); //5
    console.log("letY fc : ", letY); //7
    // console.log("letZ fc : ", letZ); //NOPE
}

letFonction();
console.log("letX dehors : ", letX); //5
// console.log("letY dehors : ", letY); //NOPE
// console.log("letZ dehors : ", letZ); //NOPE

// Le let a un niveau de portée supérieur au var, une variable déclarée avec un let dans un if, for, switch, n'existera que dans ce bloc, contrairement au var

// * Types primitifs
// Number (int, float, double, decimal)
// String 
// Boolean
// Undefined
// Null (Réprésenter le rien, que quelque chose est nullable, on veut mettre une valeur dedans mais on ne sait pas encore laquelle ou alors on veut représenter le fait qu'elle n'en aura jamais)
// Object (tableau, objet, date etc...)
let monNombreBis = 42;
console.log( typeof monNombreBis ); 

console.log(typeof letFonction);

console.clear();
// * Convertir X -> Chaine
let monNombre = 42;
console.log(monNombre.toString());
console.log(monNombre.toString(2)); //Binaire
console.log(monNombre.toString(16)); //Hexadecimal

let maDate = new Date();
console.log(maDate.toString());
let monBoolean = false;
console.log(monBoolean.toString());
let maChaineBis = "Oui";
console.log(maChaineBis.toString());
let monTab = [1, "Pouet", 3, 4]
console.log(monTab.toString());


// * Convertir Chaine -> Number
// En Int
let entier = parseInt("bleb") // NaN
let entier2 = parseInt("12") // 12
let entier3 = parseInt("12.84") // 12 // attention qu'il ne fait pas d'arrondi, il découpe juste la partie entière du nombre
// Si vous voulez arrondir
console.log(Math.floor(12.6)); //Arrondi à l'entier inférieur // 12
console.log(Math.ceil(12.4)); //Arrondi à l'entier supérieur // 13
console.log(Math.round(12.5)); //Arrondi à l'entier le plus proche //13
console.log(Math.round(12.4)); //Arrondi à l'entier le plus proche //12

// Avec les bases
let entier4 = parseInt('10101', 2)
console.log(entier4);
let entier5 = parseInt('45f', 16)
console.log(entier5);

// En Float
let float = parseFloat("12.56"); //Attention votre chaine devra contenir un . et pas une ,
console.log(float);

