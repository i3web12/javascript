//#region Récap Tableaux
console.log("Récap tableaux");
//objet
let monTableauAssociatif = {}

//Déclarer un tableau
let monTableau = [ "Blabla" , 12 , 2];
//Accéder à une valeur (via l'opérateur d'accès)
console.log( monTableau[1] ); //12
//Ajouter un élément à la fin du tableau
monTableau.push("A la fin");
//Inverser un tableau
monTableau.reverse();
//Trier (nombre par ordre croissant ou décroissant (a, b) => a - b  ou (a, b) => b - a)
monTableau.sort((a, b) => a - b);
//Pour afficher sous forme de chaine et choisir le séparateur
monTableau.join(" ");
//Pour supprimer un élément du tableau
//splice/slice
monTableau.splice(2, 1); //Supprime 1 seul élément à partir de l'indice 2 (va donc supprimer 2)
//#endregion Récap Tableaux

//#region Chaines
console.clear();
console.log("Les chaines");
//3 façons de les noter
let maChaine1 = "Aujourd'hui nous sommes mercredi, c'est la reprise 😢";
let maChaine2 = 'Aujourd\'hui nous sommes mercredi, c\'est la reprise 😢';
    //L'interpolation
let jourSemaine = 'mercredi';
let jourMois = 16;
let mois = 'août';
let maChaine3 = 'Aujourd\'hui nous sommes ' + jourSemaine + ' ' + jourMois + ' ' + mois + ' !' ;
let maChaine4 = `Aujourd'hui nous sommes ${jourSemaine} ${jourMois} ${mois} !`;

//On peut voir la chaine un peu comme un tableau, la première lettre est à l'indice 0 et la dernier taille de la chaine -1 SAUF qu'elle est dite immutable
console.log(maChaine4[0], maChaine4.length);

//On peut la parcourir comme un tableau mais elle n'est pas modifiable
maChaine4[10] = "P";
console.log(maChaine4);

// Les méthodes utiles : 
"gojgoer" + "frejfierfe"
"gojgoer".concat("frejfierfe");
//indexOf, lastIndexOf
let maChaine5 = 'Bonjour on est bons';
//exemple pour trouver toutes les positions de "on"
let i = 0;
let indices = [];
while(maChaine5.indexOf('on', i) !== -1) {
    let indiceTrouve = (maChaine5.indexOf('on', i));
    indices.push(indiceTrouve);
    i = indiceTrouve+1;
}
console.log(indices);

let maChaine6 = '\t\t\t\t\t\tnfierfnierhnfier. \nfojerifejnifjer';
console.log(maChaine6);
//maChaine6 = maChaine6.replaceAll("\t", "");
//console.log(maChaine6);
//maChaine6 = maChaine6.replaceAll("\n", "");
// console.log(maChaine6);

//ou en une fois avec une regex
let maRegEx = /[\t\n]/g
maChaine6 = maChaine6.replaceAll(maRegEx, "")
console.log(maChaine6);

console.log(maChaine5.search('on'));

//Split
let monAvatar = 'monAvatar.png';
let monAvatarDecoupe = monAvatar.split('.');
console.log("Nom Fichier", monAvatarDecoupe[0]);
console.log("Nom Fichier", monAvatarDecoupe[1]);

console.log(monAvatar.substring(0, 9));

let prenomATest = "Aude";
let prenomRentre = "      aude";
console.log(prenomATest.trim().toLowerCase() === prenomRentre.trim().toLowerCase());

//#endregion Chaines