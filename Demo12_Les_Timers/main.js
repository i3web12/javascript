console.log("LES TIMERS");

// Deux types :
// setTimeout
// On l'utilise pour déclencher UNE action, au bout d'un certain temps
// setTimeout( fonctionADeclencher, tempsEnMillisecondesAvantDeclenchement)
setTimeout(() => {
    console.log("Coucou");
}, 2000)

// setInterval
// On l'utilise pour déclencher une action PLUSIEURS fois, tous les X temps
// setInterval( fonctionADeclencherTousLesXTemps, tempsEntreChaqueAction)
// let compteur = 0;
// setInterval(() => {
//     compteur++;
//     console.log(compteur);
// }, 1000)

// Soucis : Ces timers sont infinis, on va donc devoir les arrêter
const COMPTEUR = document.getElementById('compteur')
const BTN_START = document.getElementById('start')
const BTN_STOP = document.getElementById('stop')

let timer;
let compteur = 0;

BTN_START.addEventListener('click', () => {
    // Quand on clique sur le bouton start, on va devoir lancer le timer
    timer = setInterval(() => {
        compteur++;
        COMPTEUR.textContent = compteur;
    }, 1000);

    BTN_START.disabled = true;
    BTN_STOP.disabled = false;
})

BTN_STOP.addEventListener('click', () => {
    // Quand on clique sur stop, on arrête le timer
    clearInterval(timer);

    BTN_START.disabled = false;
    BTN_STOP.disabled = true;
})

// ---- EXEMPLE D'UTILISATION :
const GUIRLANDE = ['jaune', 'bleu', 'rouge', 'vert'];
let currentColor = 0;

const DIV_NOEL = document.querySelector('.noel');

setInterval(() => {
    //On enlève la classe actuelle
    DIV_NOEL.classList.remove(GUIRLANDE[currentColor]);
    currentColor++;
    //On vérifie si on est arrivé au bout du tableau
    if(currentColor >= GUIRLANDE.length){
        //Si oui, on redémarre de 0
        currentColor = 0;
    }
    DIV_NOEL.classList.add(GUIRLANDE[currentColor]);
}, 500);