const INPUT_NAME = document.getElementById("name")
const COUNTRY_SELECT = document.getElementById("country")

const BTN_AGIFY = document.getElementById("agify")
const BTN_AGIFY2 = document.getElementById("agify2")
const BTN_AGIFY3 = document.getElementById("agify3")

const DIV_RESULT = document.getElementById("resultat")
const DIV_RESULT2 = document.getElementById("resultat2")
const DIV_RESULT3 = document.getElementById("resultat3")


BTN_AGIFY.addEventListener("click", () => {
    let name = INPUT_NAME.value;
    let country_code = COUNTRY_SELECT.value;

    //Si les deux sont remplis :
    if(name != '' && country_code != '')
    {
        console.log(`Nom : ${name} | CountryId : ${country_code}`);

        // ! Faire la requête vers l'API

        //? 1) Construction de l'objet xhr
        const xhr = new XMLHttpRequest();

        //? 2) Rajouter un listener sur le changement de statut de notre objet xhr
        xhr.onreadystatechange = () => {
            //#region ReadyState - Etat de la requête
            //0 -> Object xhr crée mais pas open
            //1 -> open ok
            //2 -> send ok
            //3 -> Traitemet en cours
            //4 -> Traitement fini
            //#endregion
            //console.log("READY STATE : ", xhr.readyState);
            //#region Status - Status de la requête
            //2xx -> ok tout s'est bien passé
            //4xx -> erreur requête (droit, structure etc)
            //5xx -> erreur serveur
            //#endregion
            //console.log("STATUS : ", xhr.status);

            //Si on veut traiter les données quand tout s'est bien passé
            if(xhr.status === 200 && xhr.readyState === 4) {
                //xhr.responseText -> Réponse de la requête mais sous forme de string
                console.log(xhr.responseText)
                //Nous souhaitons plutôt récupérer un objet js
                let data = JSON.parse(xhr.responseText)
                console.log(data);
                DIV_RESULT.textContent = `${name} a probablement autour de ${data.age} ans basé sur un résultat de ${data.count} pour le code pays ${data.country_id}`;
            }
        }

        //? 3) Ouvrir la requête (Paramétrer la requête)
        //#region explication method open
        // open(method, url, sync)
        // method : string -> Méthode avec laquelle on appelle l'API
            //GET : recevoir des données
            //POST : envoyer des données ()
            //PUT & PATCH : modifier des données
            //DELETE : supprimer une donnée
        // url : string -> le lien de votre requête (api)
        // async : bool -> Si vrai, la requête sera asynchrone, si faux, la requête sera synchrone
            //synchrone : Tout le site est bloqué pendant que la requête se fait
            // asynchrone : La requête se fait dans son coin et le site est toujours utilisable
        //#endregion
        xhr.open("GET", `https://api.agify.io/?name=${name}&country_id=${country_code}`, true)

        //? 4) Envoie de la requête
        xhr.send()
        //Si method = POST (ou PUT/PATCH), on passe les données à envoyer/modifier en paramètre
        //xhr.send("mes données")
    }
})

BTN_AGIFY2.addEventListener("click", async () => {
    let name = INPUT_NAME.value;
    let country_code = COUNTRY_SELECT.value;

    //Si les deux sont remplis :
    if(name != '' && country_code != '')
    {
        //Version avec then
        // fetch(`https://api.agify.io/?name=${name}&country_id=${country_code}`).then(res => {
        //     console.log("FETCH ", res);
        //     res.json().then(data => {
        //         console.log(data);
        //         DIV_RESULT2.textContent = `${name} a probablement autour de ${data.age} ans basé sur un résultat de ${data.count} pour le code pays ${data.country_id}`;
        //     })
        // })

        //Version avec async/await
        try {
            let res = await fetch(`https://api.agify.io/?name=${name}&country_id=${country_code}`)
            let data = await res.json()
            DIV_RESULT2.textContent = `${name} a probablement autour de ${data.age} ans basé sur un résultat de ${data.count} pour le code pays ${data.country_id}`;
        }
        catch(err) {
            console.log(err);
        }
        
    }
})

BTN_AGIFY3.addEventListener("click", async () => {
    let name = INPUT_NAME.value;
    let country_code = COUNTRY_SELECT.value;

    //Si les deux sont remplis :
    if(name != '' && country_code != '')
    {
        // axios.get(`https://api.agify.io/?name=${name}&country_id=${country_code}`).then(res => {
        //     console.log(res.data);
        //     DIV_RESULT3.textContent = `${name} a probablement autour de ${res.data.age} ans basé sur un résultat de ${res.data.count} pour le code pays ${res.data.country_id}`;
        // }).catch(err => console.log(err));

        try {
            let res = await axios.get(`https://api.agify.io/?name=${name}&country_id=${country_code}`)
                console.log(res.data);
                DIV_RESULT3.textContent = `${name} a probablement autour de ${res.data.age} ans basé sur un résultat de ${res.data.count} pour le code pays ${res.data.country_id}`;
        }
        catch(err) {
            console.log(err);
            DIV_RESULT3.textContent = `ERREUR REQUETE`
        }
        
    }
})