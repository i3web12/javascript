const P_BIENVENUE = document.getElementById('bienvenue');
const BTN_JOUR_NUIT = document.getElementById('jour-nuit');

BTN_JOUR_NUIT.addEventListener('click', () => {
    // Est-ce que mon paragraphe possède la class jour ?
    // if(P_BIENVENUE.classList.contains('jour')) {
    //     //Si oui, je l'enlève
    //     P_BIENVENUE.classList.remove('jour');
    //     //Et je rajoute nuit à la place
    //     P_BIENVENUE.classList.add('nuit');
    // }
    // //Sinon, je fais l'inverse
    // else {
    //     P_BIENVENUE.classList.remove('nuit');
    //     P_BIENVENUE.classList.add('jour');
    // }

    P_BIENVENUE.classList.toggle('jour');
    P_BIENVENUE.classList.toggle('nuit');

})

// Pour récupérer la liste des class d'un élément
// -> Vous donne un tableau avec toutes les classes de l'élement
// ELEM.classList
// ELEM.classList.contains('nomClass') -> Renvoie un boolean si oui ou non la classe est présente
// ELEM.classList.add('nomClass') -> Ajoute la classe
// ELEM.classList.remove('nomClass') -> Enlève la classe
// ELEM.classList.toggle('nomClass') -> Si nomClass est présent, ça remove la classe, si nomClass n'est pas présent, ça l'ajoute

console.log(P_BIENVENUE.classList);
// Attention className -> Renvoie une chaine de caractère avec les noms des classes appliquées sur l'élément
console.log(P_BIENVENUE.className);


//---------------- DEMO ONCHANGE
const INPUT = document.getElementById('mon-input');

INPUT.addEventListener('input', (e) => {
    console.log(e.target.value);
    let taille = e.target.value.length;
    if (taille >= 0 && taille <= 5) {
        INPUT.style.backgroundColor = 'darkred';        
    }
    else if (taille > 5 && taille <= 10) {
        INPUT.style.backgroundColor = 'orange';
    } 
    else if(taille > 10 && taille <= 15) {
        INPUT.style.backgroundColor = 'darkgreen';
    }
})


const PETIT_POUET = document.getElementById('petit-pouet')
PETIT_POUET.style.top = "0px";
PETIT_POUET.style.left = "0px";

let oldX;
let oldY;

let petitPouetHold = false;

PETIT_POUET.addEventListener('mousedown', (e) => {
    petitPouetHold = true;
    oldX = e.pageX;
    oldY = e.pageY;
})

window.addEventListener('mouseup', () => {
    petitPouetHold = false;
})
PETIT_POUET.addEventListener('mousemove', (e) => {
    if(petitPouetHold) {
        
        if(e.pageX > oldX) {
            PETIT_POUET.style.left = +PETIT_POUET.style.left.replace("px", "") + (e.pageX - oldX) + 'px';
            oldX = e.pageX;
        }
        if(e.pageX < oldX) {
            PETIT_POUET.style.left = +PETIT_POUET.style.left.replace("px", "") - (oldX - e.pageX) + 'px';
            oldX = e.pageX;
        }
        if(e.pageY > oldY) {
            PETIT_POUET.style.top = +PETIT_POUET.style.top.replace("px", "") + (e.pageY - oldY) + 'px';
            oldY = e.pageY;
        }
        if(e.pageY < oldY) {
            PETIT_POUET.style.top = +PETIT_POUET.style.top.replace("px", "") - (oldY - e.pageY) + 'px';
            oldY = e.pageY;
        }
    }
})