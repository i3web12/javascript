const P_MESSAGE = document.getElementById('message');
const BUTTON_BJR = document.getElementById('bjr');

// 1ère façon de rajouter un évènement :
// ! A éviter si possible
function direBonjour() {
    P_MESSAGE.innerText = "Bonjour"
}

// 2ème façon :
// ! A éviter
// BUTTON_BJR.onclick = direBonjour;

// 3ème façon : la meilleure
// ! A utiliser + celle que vous trouverez en majorité dans les tutos
//addEventListerner(nomEvent, callback à déclencher)

// callback = variable qui contient une fonction

//Attention à ne pas mettre les parenthèses, sinon votre fonction va se déclencher à la lecture du code, on doit juste lui fournir le nom de la fonction qui devra être déclenchée quand l'évènement surviendra
//BUTTON_BJR.addEventListener('click', direBonjour());

BUTTON_BJR.addEventListener('click', direBonjour);

// Dans la majorité des cas, on ne va pas créer une fonction pour la mettre dans notre eventListener, on va plutôt directement écrire des fonctions fléchées à l'intérieur
BUTTON_BJR.addEventListener('click', () => {
    P_MESSAGE.innerText = "Bonjour"
})