console.log('P\'tit récap DOM : ');

// Récupérer des éléments du DOM :
    // En fonction de l'id :
const LI_HTML_CSS = document.getElementById('htmlcss');
console.log("getElementById : ", LI_HTML_CSS);

    // Premier élément qui correspond à la query :
const LI_AUDE = document.querySelector('.prof')
console.log("querySelector avec .prof: ", LI_AUDE);

    // Tous les éléments qui correspondent à la query :
const LI_FORMATEURS = document.querySelectorAll('.prof');
console.log("querySelectorAll avec .prof : ", LI_FORMATEURS);

    // Exemple d'une query plus avancée :
const LI_PAIR = document.querySelectorAll('.prof:nth-child(even)')
console.log('querySelectorAll plus avancé pour récup .prof pair : ', LI_PAIR);

//Récupérer/Modifier des éléments du DOM :
    // Récupérer/Modifier le contenu
    // Récupérer
// console.log( LI_AUDE.innerText )
console.log( LI_AUDE.innerHTML )
    // Modifier
// LI_AUDE.innerText = "<p>Samuel</p>";
// LI_AUDE.innerHTML = "<p>Samuel</p>";
    // Ajouter à ce qui existe déjà
// LI_AUDE.innerText = LI_AUDE.innerText + ' Beurive';
LI_AUDE.innerText += ' Beurive';

    // Récupérer/Modifier les attributs
// LI_AUDE.setAttribute('id', 'prof-aude'); //Ajouter/Modifier attribut

LI_AUDE.id = 'prof-aude';

// console.log(LI_AUDE.getAttribute('id')); //Récupérer valeur de l'attribut
console.log( LI_AUDE.id );

// LI_AUDE.removeAttribute('id'); //Supprimer attribut

//Exemple avec une image
const IMG_CATMEME = document.getElementById('cat-meme');

IMG_CATMEME.src = 'https://i.kym-cdn.com/entries/icons/facebook/000/027/852/Screen_Shot_2018-12-12_at_1.02.39_PM.jpg'


// Ajouter des éléments au DOM :
// Version longue mais safe :
const NEW_LI = document.createElement('li');
// Setup l'élément
NEW_LI.innerText = 'Alexandre';

// L'ajouter à un autre élément
const LIST_PROF = document.getElementById('list-prof');
// LIST_PROF.appendChild(NEW_LI);
// LIST_PROF.append('Pour', 'Ajouter', 'Plein', 'Denfant');

// Avant la liste (insertBefore)
// LIST_PROF.insertAdjacentHTML('beforebegin', NEW_LI.outerHTML)

//En premier élément de la liste
// LIST_PROF.insertAdjacentHTML('afterbegin', NEW_LI.outerHTML)

//En dernier élément de la liste (appendChild)
// LIST_PROF.insertAdjacentHTML('beforeend', NEW_LI.outerHTML)

// Après la liste 
// LIST_PROF.insertAdjacentHTML('afterend', NEW_LI.outerHTML)

    //Version rapide 
    // ! Attention à l'injection Js, ne n'utilisez que pour construire du HTML dans données exterieures
LIST_PROF.innerHTML += '<li id="alex" class="prof">Alexandre</li>';