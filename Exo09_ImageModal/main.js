console.log("Exo modal");
//#region Etape 1 :
// Construire le html avec 
//   . les images
//   . la modal (avec une image dont le src est vide, un titre, une croix pour     fermer)
//#endregion Etape 1 

// Etape 2 :
// En JS, récupérer chacune des images et ajouter un eventListener pour qu'au click, la modal s'affiche (la modal est toujours vide à ce stade)
const IMAGES = document.querySelectorAll(".galery img");
const MODAL = document.querySelector(".modal");
const CLOSE = document.querySelector(".modal span");
const IMAGE_MODAL = document.querySelector(".modal img");
const MODAL_H2 = document.querySelector(".modal h2");

console.log(IMAGES);

// Si mon tableau existe déjà 
//for ... of ... (Pour chaque élement de mon tableau)
//foreach (Pour chacun -> Méthode qui s'utilise sur un tableau)
//IMAGES.forEach(IMAGE => console.log(IMAGE))

for(const IMAGE of IMAGES) {
    // console.log(IMAGE);
    IMAGE.addEventListener('click', () => {
        //Au click sur une image, on enlève la classe hidden (celle qui fait que notre modal est cachée)
        MODAL.classList.remove('hidden');

        //Récupérer la source de l'image
        // console.log(IMAGE.getAttribute('src'));
        // IMAGE.src

        // Modifier la source d'une image
        //IMAGE_MODAL.setAttribute("src", IMAGE.src)
        IMAGE_MODAL.src = IMAGE.src
        IMAGE_MODAL.alt = IMAGE.alt

        MODAL_H2.textContent = IMAGE.alt;

        IMAGE_MODAL.classList.add('img-popping');
    })
}

// En JS, récupérer la p'tite croix pour ajouter un eventListener pour qu'au click, la modal se désafficher
CLOSE.addEventListener('click', () => {
    MODAL.classList.add('hidden');
    IMAGE_MODAL.classList.remove('img-popping');

})

// Etape 3 :
// Rajouter, au click sur une image, le fait de récupérer la source de l'image cliquée
// (Indice (e) => e.target.src) (Testez avec des console.log ;) )
// Quand c'est bon, remplacer la source de l'image de la modal par celle récupérée juste au dessus
 
// Etape 3 bis :
// Faire pareil avec le alt de l'image clickée dans le h2 de la modal

// ----- BONUS 
// Quand on clique sur la fenêtre, on veut fermer la modal 
// MAIS uniquement si c'est sur la modal quand elle est ouverte
window.addEventListener('click', (e) => {
    if(e.target == MODAL) {
        MODAL.classList.add('hidden');
        IMAGE_MODAL.classList.remove('img-popping');
    }
})