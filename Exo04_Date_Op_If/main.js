// A l'aide des prompts et alert, demander à l'utilisateur d'entrer son jour de naissance, son mois de naissance, son année de naissance
// Grâce à la fonction de formatage, afficher "Vous êtes né un [jourSemaine] [jourMois] [mois] [année]"
let day = parseInt(prompt("Entrez votre jour de naissance"))
let month = parseInt(prompt("Entrez votre mois de naissance (en nombre)"))
let year = parseInt(prompt("Entrez votre année de naissance"))

let birthday = new Date(year, month-1, day);

alert(`Vous êtes né un ${birthday.toLocaleDateString('fr-BE', { weekday : "long", day : "2-digit", month : "long" , year : "numeric" })}`)

// Bonus (utilisation du if) : 
// (Sans librairie)
// Lui afficher "Il reste [X] jours avant votre anniversaire"
// (Regardez du côté de getTime() qu'on n'a pas regardé ensemble
// Bonus du bonus : afficher le s ou non à jours

//On aura besoin de comparer à la date du jour
let today = new Date()
//Vérifier si l'anniversaire est déjà passé :
//On setup l'année de l'anniv à l'année en cours
birthday.setFullYear(2023);

//Si c'est pile le même mois et le même jour -> Anniversaire !
 if(birthday.getMonth() === today.getMonth() && birthday.getDate() === today.getDate()) {
        alert("Bon anniversaire 🥳");
} 
//Sinon, si l'anniversaire est plus petit que la date du jour, il est déjà passé
else if(birthday < today) {

    //On ajoute alors un an pour notre calcul
    birthday.setFullYear(2024);
    //(birthday.getTime() - today.getTime()) -> Différence en ms entre deux dates
    // Si on veut récupérer le nombre de jours dedans, on divise ces ms par 24 (h) * 60 (m) * 60 (s) * 1000 (ms)
    let daysBegoreBd = Math.ceil((birthday.getTime() - today.getTime()) / (24 * 60 * 60 * 1000))
    alert(`Il reste ${daysBegoreBd} jour${daysBegoreBd > 1 ? 's' : ''} avant votre anniversaire !`)

}   
//Sinon c'est que l'anniversaire est plus grand donc pas encore arrivé
 else {
    let daysBegoreBd = Math.ceil((birthday.getTime() - today.getTime() ) / (24 * 60 * 60 * 1000))
    alert(`Il reste ${daysBegoreBd} jour${daysBegoreBd > 1 ? 's' : ''} avant votre anniversaire !`)
 }
