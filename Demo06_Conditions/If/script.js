let jourSemaine = 1

// Permet d'évaluer un résultat a true ou false
// opérateur => + - * / =
// opérateur => < > == !=
// opérateur => && ||


// if (jourSemaine == 1) {
//     console.log("Lundi")
// }
// else if (jourSemaine == 2) {
//     console.log("Mardi")
// }
// else if (jourSemaine == 3) {
//     console.log("Mercredi")
// }
// else if (jourSemaine == 4) {
//     console.log("Jeudi")
// }
// else if (jourSemaine == 5) {
//     console.log("Vendredi")
// }
// else if (jourSemaine == 6) {
//     console.log("Samedi")
// }
// else if (jourSemaine == 7) {
//     console.log("Dimanche")
// }
// else{
//     console.log(jourSemaine + ": Ne correspond a aucun jour de la semaine")
// }

switch (jourSemaine) {
    case 1:
        console.log("Lundi")
        break;
    case 2:
        console.log("Mardi")
        break;
    case 3:
        console.log("Mercredi")
        break;
    case 4:
        console.log("Jeudi")
        break;
    case 5:
        console.log("Vendredi")
        break;
    case 6:
        console.log("Samedi")
        break;
    case 7:
        console.log("Dimanche")
        break;
    default:
        console.log(jourSemaine + ": Ne correspond a aucun jour de la semaine")
        break;
}