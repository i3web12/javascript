console.log("Asynchronicité - Callback - Les promesses");
// Vocabulaire :
// Synchrone -- Quand on va lancher une tâche (exemple, une requête vers une BDD), aucune autre action n'est possible
// Asynchrone -- Au contraire, une tâche asynchrone, va s'effectuer en fond et on pourra faire toute autre action à côté
// Typiquement, nos requêtes vers des API devront être gérées de façons asynchrones puisqu'elles prennent du temps

// #region Simulation système asynchrone -> sans rien = FAIL
// function preparer(nbOeuf) {

//     setTimeout(() => {
//         // On va avoir une chance sur 3 de rater
//         let chance = Math.floor(Math.random() * 3 ) + 1; //nbAléatoire entre 1 et 3
//         if(chance === 2) {
//             console.log("Onon, les oeufs étaient pourris 🤢")
//         }
//         else {
//             console.log(`Nous avons bien battu les ${nbOeuf} oeufs 🥚`);
//         }
//     }, 4000 )
// }

// function cuire() {

//     setTimeout(() => {
//         // On va avoir une chance sur 3 de rater
//         let chance = Math.floor(Math.random() * 3 ) + 1; //nbAléatoire entre 1 et 3
//         if(chance === 2) {
//             console.log("Onon, l'omelette a brulé 🔥")
//         }
//         else {
//             console.log("L'omelette est prête ! 🍳");
//         }
//     }, 2000 )
// }

// function servir() {

//     setTimeout(() => {
//         // On va avoir une chance sur 3 de rater
//         let chance = Math.floor(Math.random() * 3 ) + 1; //nbAléatoire entre 1 et 3
//         if(chance === 2) {
//             console.log("Onon, le serveur a glissé avec notre omelette ")
//         }
//         else {
//             console.log("Bon appétit !");
//         }
//     }, 1000 )
// }

// preparer(6);
// cuire();
// servir();
//#endregion

// #region Simulation système asynchrone géré avec les callback
// Avec le principe de callback, on va fournir la/les fonction/s à appeler à la fin du traitement et en cas de réussite de la fonction dont dépend le résultat 

// function preparer(nbOeuf, fonctionALancer /*cuire*/, fonctionALancer2/*servir*/) {

//     setTimeout(() => {
//         // On va avoir une chance sur 3 de rater
//         let chance = Math.floor(Math.random() * 3 ) + 1; //nbAléatoire entre 1 et 3
//         if(chance === 2) {
//             console.log("Onon, les oeufs étaient pourris 🤢")
//         }
//         else {
//             console.log(`Nous avons bien battu les ${nbOeuf} oeufs 🥚`);
//             fonctionALancer(fonctionALancer2); // Déclenchement de la fonction qui sera stockée dans fonctionALancer
//             //cuire(servir)
//         }
//     }, 4000 )
// }

// function cuire(fonctionALancer /*servir*/) {

//     setTimeout(() => {
//         // On va avoir une chance sur 3 de rater
//         let chance = Math.floor(Math.random() * 3 ) + 1; //nbAléatoire entre 1 et 3
//         if(chance === 2) {
//             console.log("Onon, l'omelette a brulé 🔥")
//         }
//         else {
//             console.log("L'omelette est prête ! 🍳");
//             fonctionALancer(); //servir()
//         }
//     }, 2000 )
// }

// function servir() {

//     setTimeout(() => {
//         // On va avoir une chance sur 3 de rater
//         let chance = Math.floor(Math.random() * 3 ) + 1; //nbAléatoire entre 1 et 3
//         if(chance === 2) {
//             console.log("Onon, le serveur a glissé avec notre omelette ")
//         }
//         else {
//             console.log("Bon appétit !");
//         }
//     }, 1000 )
// }

// preparer(6, cuire, servir);
//#endregion 

// Simulation système asynchrone géré avec les promesses
//#region  Demo avec division
// function division(nombre1, nombre2) {
//     //On renvoie une promesse
//     // (resolve, reject) => {}  
//     //      -> resolve : fonction à déclencher quand tout est ok
//     //      -> reject : fonction à déclencher quand raté
//     return new Promise((resolve, reject) => {
//         // Si on essaie de diviser par 0, on renvoie une erreur
//         if(nombre2 == 0) {
//             reject("Error : Division par 0 impossible")
//         }
//         // Sinon, on fait bien la division
//         else {
//             resolve(nombre1 / nombre2)
//         }
//     })
// }

// division(12, 0)
//     .then((result) => { console.log("Division : "+ result) }) 
//     //Déclenché quand tout se passe bien (resolve)
//     .catch((error) => { console.log(error)}) 
//     //Déclenché quand reject
//     .finally(() => { console.log("Fin de la division"); }) 
//     //Déclenché à la fin, que ce soit reject ou resolve

// Version raccourcie :
// async function operation() {
//     try {
//         let resultat = await division(12, 0);
//         console.log("Division :" + resultat)
//     }
//     catch(error) {
//         console.error(error)
//     }
//     //finally
//     console.log("Division finie")
// }
// operation()
//#endregion

//#region Demo avec l'omelette

function preparer(nbOeuf) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            // On va avoir une chance sur 3 de rater
            let chance = Math.floor(Math.random() * 3) + 1; //nbAléatoire entre 1 et 3
            if (chance === 2) {
                reject("Onon, les oeufs étaient pourris 🤢") // La promesse est rejetée
            }
            else {
                resolve(`Nous avons bien battu les ${nbOeuf} oeufs 🥚`);
            }
        }, 4000)
    })
}

function cuire() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let chance = Math.floor(Math.random() * 3) + 1; //nbAléatoire entre 1 et 3
            // On va avoir une chance sur 3 de rater
            if (chance === 2) {
                reject("Onon, l'omelette a brulé 🔥")
            }
            else {
                resolve("L'omelette est prête ! 🍳");
            }
        }, 2000);
    })
}

function servir() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            // On va avoir une chance sur 3 de rater
            let chance = Math.floor(Math.random() * 3) + 1; //nbAléatoire entre 1 et 3
            if (chance === 2) {
                reject("Onon, le serveur a glissé avec notre omelette ")
            }
            else {
                resolve("Bon appétit !");
            }
        }, 1000)
    })
}

//Version longue :
// preparer(6)
//     .then((res) => {
//         console.log(res);
//         cuire()
//             .then((res) => {
//                 console.log(res);
//                 servir()
//                     .then(res => {
//                         console.log(res);
//                     })
//                     .catch(err => console.error(err))
//             })
//             .catch(err => console.error(err))
//     })
//     .catch(err => console.error(err));

// Version raccourcie
async function monOmelette() {
    try {
        let prep = await preparer(); 
        //On attend que preparer finisse (await) si ça finit bien, on passe à la ligne suivante, sinon, on passe dans le bloc catch
        console.log(prep);
        let cui = await cuire();
        //Same 
        console.log(cui);
        let ser = await servir();
        //Same
        console.log(ser);
    }
    catch(err) {
        console.error(err)
    }
}

monOmelette();

