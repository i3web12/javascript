// ?  un script Js qui demande à l'utilisateur d'entrer son nom, ensuite son prénom, ensuite son âge et affiche "Bonjour [nom] [prenom], vous allez bientôt avoir [age + 1] ans"
// Rappel : Pour récupérer une valeur : prompt
// 	Pour afficher : alert
// 	Piège : prompt renvoie un string ;)
// let : mot-clef permettant de déclarer une variable
// let firstname = prompt("Entrez votre prénom :")
// let lastname = prompt("Entrez votre nom :")
// let age = parseInt( prompt("Entrez votre âge :") )

// alert("Bonjour " + firstname + " " + lastname + ", vous allez bientôt avoir " + (age + 1) + " ans !")
//Interpolation (ALTGR + µ (* 2 comme le ^^)) `` 
// alert(`Bonjour ${firstname} ${lastname}, vous allez bientôt avoir ${age+1} ans !`)


// ? Calcul de la TVA
// Écrire un programme qui :
// 1. Demande à l’utilisateur un prix unitaire hors taxe d’un livre
// 2. Demande à l’utilisateur la quantité de livre
// 3. Calcule et affiche le prix total TTC de la commande, en utilisant une TVA de 21%
// Pour interagir avec l’utilisateur, vous utiliserez les fonctions d’entrée/sortie prompt() et alert(). 
let bookPrice = parseFloat( prompt("Entrez le prix unitaire d'un livre :").replace(",", ".") );
// Replace("x","y") -> Remplace la lettre x par y dans la chaine
let bookQty = parseInt( prompt("Entrez la quantité de livres :") );

let priceWithoutTVA = bookPrice * bookQty;
let priceWithTVA1 = priceWithoutTVA * 1.21;
let priceWithTVA2 = priceWithoutTVA + (priceWithoutTVA * 0.21);
alert(`La commande de ${bookQty} livres à ${bookPrice}€ fera ${priceWithTVA1.toFixed(2)}€ TTC`)

//toFixed(X) -> Permet d'arrondir à X chiffres après la virgule