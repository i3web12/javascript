// #region For V_1

// // idéal lorque que l'on connais le nombre de tour a résalier
// // bloc déclaratif => compteur 
// // bloc conditionnel => permet de controlé la prochaine itération
// // bloc incrémentation => controle le nonbre de pas par tour
// console.log("Demos For")
// for (let i = 1; i <= 3; i++) {
//     console.log(`Tour de boucle ${i}`)
// }
// console.log("---------------------")
// // déclartion d'un tableau
// let monTabFruit = ["Pomme","Poire","Banane"]
// let objParticipant = { 1 : "Jean",  2 : "Claude",  3 : "Pierre"}
// let TabParticipant = 
// [
//     { Nom: "El jilali", Prenom: "Yousra" }, // élément 0
//     { Nom: "La", Prenom: "Yous" }           // élément 1
// ]

// #endregion

// #region For V_2

// console.log("Demos For V_2 (Structure)")
// // For permet de parcourir une collection
// for(toto in monTabFruit){
//     // le fruit (item) nous permet d'acceder a l'index
//     console.log(monTabFruit[toto])
// }

// // for(participant in objParticipant){
// //     console.log(`${participant} - ${objParticipant[participant]}`)
// // }

// for(participant in TabParticipant){
//     console.log(`le Nom du participant ${participant} est ${TabParticipant[participant].Nom}`)
// }

// #endregion

// #region While
// ! test antérieur
// lorsque nous implémentons un while il est nécesaire de déclarer la variable a l'extérieur
let nombre = 5;

while (nombre >= 1) {
    nombre--; // Ne sourtout pas oublier l'incrémentation sous peine de se retrouver dans une boucle infinie !
    console.log(nombre);
}

// démo récupération de valeur

let temp;

console.log("Encodez une température entre 1 et 10")
temp = parseInt(prompt())
while (temperature < 1 || temperature > 10) {
    console.log("Encodez une température entre 1 et 10")
    temp = parseInt(prompt())
}

// #endregion

// #region DoWhile
// ! test posterieurs
let temperature;
// Permet d'execté une série dinstruction avant d"évaluer la condition => pertinent lorque nous récupérons une valeur de l'utilisateur
do {
    temperature = parseInt(prompt("Veuillez sasir une température entre 0 et 10°"));
} while (temperature <= 0 || temperature >= 10);

console.log("La température est " + temperature)

// #endregion 

