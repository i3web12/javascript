// ? Créer une procédure qui va recevoir en paramètres un prénom, un nom et afficher (alert) "Bienvenue [prenom] [nom] !"
// function welcome(prenom, nom) {
//     alert(`Bienvenue ${prenom} ${nom}`);
//     //Pas de retour
// }

//Autre façon de faire :
const welcome = (prenom, nom) => {
    alert(`Bienvenue ${prenom} ${nom}`);
}

//Appel de la fonction avec différentes valeurs
welcome('Aude', 'Beurive');
welcome('Aurélien', 'Strimelle');

// ? Créer une fonction qui reçoit une langue en paramètre et renvoie bonjour en fonction de la langue transmise (fr -> Bonjour, en -> Hello, es -> Hola etc)
function direBonjour(langue) {
    // let phrase;
    // Soit if, soit switch, si plus de 3 conditions -> switch
    switch(langue) {
        case "fr":
           //phrase = "Bonjour";
           //break;

           return "Bonjour"
        case "en" :
            // phrase = "Hello"
            // break;

            return "Hello"
        case "es" :
            // phrase = "Hola"
            // break;
            
            return "Hola"
        case "it" : 
            // phrase = "Buongiorno"
            // break;

            return "Buongiorno"
        default :
            // phrase = "Wtf j'ai rien compris"
            // break;

            return "Wtf j'ai rien compris"
    }
    // return phrase;
}

let lang = prompt("Choisir une langue fr) Français en) Anglais es) Espagnol it) Italien");
let bjr = direBonjour(lang);
alert(bjr);

// Same plus court :
// alert( direBonjour(prompt("Choisir une langue fr) Français en) Anglais es) Espagnol it) Italien")) );

// ? Créer une fonction, qui reçoit en paramètre un nombre de joueurs. Pour chacun des joueurs, elle demande à l'utilisateur d'entrer une note (bonus : vérifier qu'elle soit bien entre 0 et 20). La fonction renvoie un tableau contenant chacune des notes
function remplirNotes(nombreJoueurs) {
    let notes = [];
    
    //for : je connais le nombre de répétitions
    for(let i = 0; i < nombreJoueurs; i++){
        //while : je ne connais pas le nombre de répétitions
        do {
            notes[i] = parseInt(prompt("Entrez une note pour le joueur n°"+ (i+1)));

        } while( !(notes[i] >= 0 && notes[i] <= 20) )  // Contraire de notre condition ok
         //while( notes[i] < 0 || notes[i] > 20 ) // Marche pareil
    }
    
    return notes;
}

let monTableauDeNotes = remplirNotes(4)
console.log(monTableauDeNotes);

// ? Créer une fonction qui reçoit un tableau (de notes) en paramètre et renvoie la moyenne des notes
function calculMoyenne(tab) {
    // let somme = 0;
    // for(let i = 0; i < tab.length; i++) {
    //     somme = somme + tab[i];
    // }

    // return somme / tab.length
    
    // Autre façon avec le reduce
    // tableau.reduce( (accumulateur, element) => accumulateur + element, valeurInitialeDeAccumulateur)
    return tab.reduce((somme, note) => somme + note , 0) / tab.length

}

alert("La moyenne est de " + calculMoyenne(monTableauDeNotes));