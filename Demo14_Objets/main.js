// ! Définir un objet :
const person1 = { 
    //clef : valeur
    //propriété : valeur
    lastname : "Beurive",
    firstname : "Aude",
    birthdate : new Date(1989, 9, 16)
}

// Manipuler un objet :
// ! Accéder à une des propriétés :
console.log( person1["firstname"] );
console.log( person1.firstname);
// ! Modifier une des propriétés :
person1.firstname = "Thomas";
console.log( person1.firstname );
// Rajouter une propriété à un objet déjà existant
person1.gender = "female";
console.log( person1 );
// Supprimer une propriété :
delete person1.gender;
console.log( person1 );

const DIV_PERSON1 = document.getElementById("person1");
DIV_PERSON1.innerText = `La personne s'appelle ${person1.firstname} ${person1.lastname} et elle est née le ${person1.birthdate.toLocaleDateString("fr-BE")}`;

// Tableaux d'objets :
const trainers = [
    { lastname : "Beurive", firstname : "Aude", gender : "female", birthdate : new Date(1989,9,16), avatar : "./assets/aude.png"},
    { lastname : "Strimelle", firstname : "Aurélien", gender : "male", birthdate : new Date(1989, 10, 1), avatar : "./assets/aurelien.png"},
    { lastname : "Chaineux", firstname : "Gavin", gender : "male", birthdate : new Date(1996, 9, 18), avatar : "./assets/gavin.png"},
];

// Fonctions utiles sur les tableaux d'objets :

// Boucle pour parcourir
trainers.forEach(trainer => console.log(trainer.firstname));
// Sert à parcourir un tableau pour le tansformer en un autre tableau
const trainersBis = trainers.map(trainer => ( { fullname : trainer.firstname + " " + trainer.lastname, gender : trainer.gender } ))
console.log(trainers);
console.log(trainersBis);
//
const trainersFemale = trainers.filter(trainer => trainer.gender == "female");
console.log("Les formateurs femmes : ", trainersFemale);
const trainersMale = trainers.filter(trainer => trainer.gender == "male");
console.log("Les formateurs hommes : ", trainersMale);

// Exemple de génération d'HTML à partir d'un tableau d'objets :
const DIV_TRAINERS = document.getElementById("trainers");

trainers.forEach(trainer => {    
    DIV_TRAINERS.innerHTML += createCard(trainer);
});

// Autre façon de faire avec map :
//DIV_TRAINERS.innerHTML = trainers.map(trainer => createCard(trainer) ).join('');


/**
 * Fonction qui prend en paramètre un formateur et construit une card html
 * @param { { lastname : string, firstname: string, gender : string, birthdate : Date, avatar : string } } trainer
 *@return string
*/
function createCard(trainer) {
    return `
        <div class="card ${trainer.gender == "female" ? "pink" : trainer.gender == "male" ? "blue" : "default"}">
            <h2> ${trainer.firstname} ${trainer.lastname}</h2>
            <img src="${trainer.avatar}" alt="la tête de ${trainer.firstname}"/>
            <p>${trainer.birthdate.toLocaleDateString("fr-BE")}</p>
        </div>
         `
}