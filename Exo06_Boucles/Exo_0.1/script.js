
console.log("-------------- Version For ---------------");
// For
let aFor = "";
for (let i = 0; i < 10; i++) {
    aFor += "A";
    console.log(aFor);
}

console.log("-------------- Version While ---------------");
// while
let compteur = 0;
let aWhile = "";
while (compteur < 10) {
    aWhile += "A";
    compteur++;
    console.log(aWhile)
}

console.log("-------------- Version Do while ---------------");
// Do While
let lettre = "";
let i = 1;
do {
    lettre += "A";
    console.log(lettre);
    i++;
} while (i <= 10);