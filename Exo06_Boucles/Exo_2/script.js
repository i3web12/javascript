const mots = ["javascript","programmation","ordinateur","développeur"]

// Choisir &léatoirement un mot de la liste
const motATrouver = mots[Math.floor(Math.random() * mots.length)];


// Initialiser motDevine avec des tirets bas pour chaque lettre du mot à deviner
let motDevine = "";
let i = 0;
while (i < motATrouver.length) {
    motDevine += "_";
    i++;
}

// définir le nomvbre d'essais autoriés
let essaiRestants = 6;

// afficher le message d'accueil
console.log("Bienvenue dans le jeu du  pendu !")
console.log("Mot à deviner :" + motDevine)

while (motDevine !== motATrouver && essaiRestants > 0) {
    // affichage des essais restant avant traitement
    console.log("Essais restant(s) :" + essaiRestants);

    // demander au joueur de saisir une lettre (conversion en minuscule)
    let lettre = prompt("Devinez une lettre :").toLowerCase();

    // variable pour vérifier si ma lettre est trouvée dans le mot 
    let lettreTrouvee = false;

    // parcourir le mot a deviner pour vérifier la lettre 
    let j = 0;
    while(j < motATrouver.length){
        if (motATrouver[j] === lettre) {
            // Remplacer le tiret du bas par la lettre correcte dans motDevine
            let nouveauMotDevine = "";
            let k = 0;
            while (k < motDevine.length) {
                if (motATrouver[k] === lettre) {
                    nouveauMotDevine += lettre;
                }else{
                    nouveauMotDevine += motDevine[k];
                }
                k++;
            }
            motDevine = nouveauMotDevine;
            lettreTrouvee = true;
        }
        j++;
    }
    // Si la lettre nes pas trouvée, réduire le nombre d'essais restants
    if (!lettreTrouvee) {
        essaiRestants--;
        console.log("Lettre incorrecte. il vous reste" + essaiRestants + " essais.")
    }

    // Afficher le mot partiellement deviné
    console.log("Mot à deviner :" + motDevine)
}

// afficher le résultat du jeu 
if (motDevine === motATrouver) {
    console.log("Bravo, vous avez deviné le mot :" + motATrouver)
}
else{
    console.log("Désolé, vous avez épuisé tous vos essais. Le mot était : " + motATrouver)
}