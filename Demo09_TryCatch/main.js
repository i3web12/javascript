console.log("TRY / CATCH ");

function division(nombre1, nombre2) {
    let resultat;

    try {
        //Essaie de faire tout ce qu'il y a dans ce bloc
        
        //Gestion des différentes erreurs
        if(isNaN(nombre1)) {
            throw("Le premier paramètre n'est pas un nombre");
        }
        if(isNaN(nombre2)) {
            throw("Le deuxième paramètre n'est pas un nombre");
        }
        if(nombre2 === 0) {
            throw("Division par 0 impossible");
        }

        // Le mot clef throw nous fait sortir du bloc try dès qu'il est rencontré et n'effectue pas le reste des opérations qui suivent        
        resultat = nombre1 / nombre2;

    }
    catch(error) {
        //Si il y a la première erreur, on l'attrape et on l'affiche
        console.error("ERREUR : ", error);
    }

    return resultat;
}

console.log(division(12, 2))


// Parenthèse sur la JSDoc
// Ne vous empêche pas de transmettre une valeur du mauvais type dans votre fonction mais donne des indications sur ce qui est attendu

// param { type } nomParamètre
// return { type } 

/**
 * @param {number} nombre1
 * @param {number} nombre2
 * @return {number}
 */
function addition(nombre1 , nombre2) {
    return nombre1 + nombre2;
}

console.log(addition(5, 2))