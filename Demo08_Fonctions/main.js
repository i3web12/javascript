console.log('LES FONCTIONS');

// Les procédures
// -> C'est une fonction qui ne renvoie pas de résultat, elle effectue un traitement
// Exemple d'une procédure que vous avez beaucoup utilisé :
// alert("Bonjour")
// Cette fonction ne renvoie pas de résultat, elle se contente d'afficher un message

// Les fonctions
// -> Renvoie toujours un résultat, qu'il faudra traiter (stocker/afficher/etc)
// Exemple d'une fonction que vous avez beaucoup utilisée :
//let userEntry = prompt("Entrez un truc")
// Cette fonction, renvoie un résultat (ce que l'utilisateur à tapé), à nous d'en faire quelque chose

// Si utilisé sur un objet : méthode
// Exemple d'une méthode que vous avez beaucoup utilisée :
console.log("Coucou")
// la méthode log, s'utilise sur l'objet console 


// Qu'on veuille créer une procédure ou une fonction, cela se fait (presque) de la même manière
// Procédure : (Pas de mot-clef return)
function nomProcedure(paramEventuel) {
    alert(paramEventuel);
}

//Fonction : (TOUJOURS le mot-clef return)
function nomFonction(paramEventuel) {
    return paramEventuel + ' a été renvoyé !';
}
// Quand on va appeler notre procédure, on a juste à l'appeler par son nom et fournir le(s) paramètre(s)
nomProcedure("Pouet procédure");

// Quand on va appeler notre fonction, on doit l'appeler par son nom, fournir le(s) paramètre(s) et faire quelque chose du résultat 
let resultat = nomFonction("Pouet fonction");
alert(resultat);

// Exo Slide : 
function maFonction(a, b, c, d, e, f) {
    // a = 7, b = 6, c = 5, d = 4, e = 3, f = 2
    let res;
    res = a + b; // res = 7 + 6 -> res = 13
    res = res * d; // res = 13 * 4 -> res = 52
    e = f; // e = 2
    res = res - e; // res = 52 - 2 -> res = 50
    return res; // renvoie 50
}

let a = 2;
let b = 3;
let c = 4;
let d = 5;
let e = 6;
let f = 7;

let g = maFonction(f, e, d, c, b, a)
alert(g) // g = 50

// Paramètre par valeur (pour tous les types simples (number, boolean, string))
function modifX(x) {
    x = 12;
    console.log("DANS LA FONCTION x VAUT ", x); //12
}

let x = 5;
modifX(x); // Au moment où la fonction est appelée, on a copié la valeur 5 présente dans x globale, dans x local de la fonction. Modifier x local ne modifie pas x global
console.log("APRES APPEL DE LA FONCTION x VAUT : ", x); //5


// Paramètre par référence
function modifTab(tab) {
    // Si j'essaie de remplacer tab par autre chose : fail
    // A la sortie de la fonction, il n'aura pas changé
    // tab = "Pouet"
    // console.log("TAB DANS FONCTION : ", tab);

    tab[1] = "Pol";
    console.log("TAB DANS FONCTION : ", tab);
}

let tab = ["Pierre", "Paul", "Jacques"];
modifTab(tab); // Au moment où la fonction est appelée, on a copié l'adresse mémoire (la référence) de tab (global) dans tab(local), ils partagent donc tous les deux, la même référence. Si je modifie l'un, ça modifie l'autre.
console.log("TAB APRES APPEL DE LA FONCTION : ", tab);


function changerDePrenom(personne, nouveauPrenom) {
    // personne.prenom = nouveauPrenom; 
    // Oui, va fonctionner, parce qu'on change un élément de notre objet, qui lui, reste intact

    personne = { nom : "Ly", prenom : nouveauPrenom}
    // Non, ne va pas fonctionner, parce qu'on essaie de changer tout l'objet et on perd la référence
}

let personne1 = {
    nom : "Beurive",
    prenom : "Aude"
}

let personne2 = {
    nom : "Strimelle",
    prenom : "Aurélien"
}

console.log("AVANT FC  : ", personne1.prenom);
changerDePrenom(personne1, "Thomas");
console.log("APRES FC  : ", personne1.prenom);

console.log("AVANT FC  : ", personne2.prenom);
changerDePrenom(personne2, "Henry");
console.log("APRES FC  : ", personne2.prenom);


let tab2 = ["Aude", "Aurélien", "Quentin"]
// fonction anonyme : Avant ES2016
tab2.forEach( function(nom) { console.log(nom) } )
// fonction anonyme (fonction fléchée) : Après ES2016
tab2.forEach( (nom) => console.log(nom) )

// On peut aussi créer des fonctions, comme ceci : 
const maFonction3 = function(param) { /*trucs à faire*/ }
maFonction3("param");

const maFonction4 = (param) => { /* trucs à faire */ }
maFonction4("param")

const changerPrenom2 = (personne, nouveauPrenom) => {
    personne.prenom = nouveauPrenom;
    console.log("Changement de prénom effectué !");
}

let personne3 = {
    nom : "Geerts",
    prenom : "Quentin"
}
console.log(personne3.prenom);
changerPrenom2(personne3, "Mélanie");
console.log(personne3.prenom);