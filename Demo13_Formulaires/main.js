//Pour récupérer le formulaire :
// 1) document.forms -> On récupère un tableau avec tous les formulaires de la pages
// Du coup, si je veux le seul formulaire de la page document.forms[0]
// 2) document.nomDuFormulaire (nomDuFormulaire = nom passé dans l'attribut name de la balise form)
const FORMULAIRE = document.monFormulaire;
console.log(FORMULAIRE);

FORMULAIRE.lastname.value = "Beurive";
FORMULAIRE.firstname.value = "Aude";
FORMULAIRE.birthdate.value = "1989-10-16";

FORMULAIRE.gender.value = "male";

const MSG_BIRTHDATE = document.getElementById('majeur');
const MSG_CONFIRM_PWD = document.getElementById('confirmbox');
const SUBMIT_BTN = document.getElementById("create");

// Pour ajouter des messages d'erreurs / validations
FORMULAIRE.birthdate.addEventListener('input', (e) => {
    console.log(e.target.value)
    // On veut vérifier si la date de naissance est ok :
    // La personne doit être majeure
    const TODAY_YEAR = new Date().getFullYear();
    const BIRTHDATE_YEAR = new Date(e.target.value).getFullYear();
    if(TODAY_YEAR - BIRTHDATE_YEAR < 18) {
        MSG_BIRTHDATE.innerText = "Vous devez être majeur";
        SUBMIT_BTN.disabled = true;
    }
    else {
        MSG_BIRTHDATE.innerText = "";
        SUBMIT_BTN.disabled = false;

    }
})

// Pour la vérification des mots de passe :
FORMULAIRE.password.addEventListener("input", (e) => {
    //Vérifier, si on le change, que confirm est déjà rempli
    if(FORMULAIRE.confirmPassword.value != "") {
        if(e.target.value !== FORMULAIRE.confirmPassword.value) {
            MSG_CONFIRM_PWD.innerText = "Les mots de passe doivent être égaux";
        }
        else {
            MSG_CONFIRM_PWD.innerText = "";
        }
    }
})
FORMULAIRE.confirmPassword.addEventListener("input", (e) => {
    // Vérifier si l'input password et l'input confirm sont égaux
    if(e.target.value !== FORMULAIRE.password.value) {
        MSG_CONFIRM_PWD.innerText = "Les mots de passe doivent être égaux";
    }
    else {
        MSG_CONFIRM_PWD.innerText = "";
    }
})


// Pour remettre à zéro le formulaire
// FORMULAIRE.reset()
