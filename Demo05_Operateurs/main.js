console.log("Les opérateurs");

// Opérateurs arithmétiques
//  +   addition
//  -   soustraction
//  *   multiplication
//  /   division (attention pas de division entière, il faudra transfo le résultat soi même)
//  %   modulo (reste de la division entière)
//  **  puissance (exposant)

let variable = 6;
variable = variable + 3;
variable += 3; //Ecriture raccourcie ↑
//Dispo pour tous les opérateurs

let x = 5;
// x++ -> le 1 est ajouté à x après calcul de y
//let y = x++ + 2 //y = 5 + 2 (7)  x = 6
// ++x -> le 1 est ajouté à x avant calcul de z
let z = ++x + 2 //z = 6 + 2 (8)   x = 6

// Opérateurs de comparaison
// >   <   >=   <=   
// == égalité  != différent  //! "7" == 7 (Ok)
// === égalité stricte !== strictement difféent //! "7" === 7 (Pas Ok)


// Opérateurs logique
// ! -> négation
// && ET   condition1 && condition2   Les deux conditions doivent être true
// || OU   condition1 || condition2   Au moins une des deux conditions doit être true
// ^^ XOR  condition1 ^^ condition2   Une seule des deux conditions doit être true

let totalPeople = 15;
let gender = 'm';

if(totalPeople > 1) {
    if(gender == 'm') {
        console.log(`Vous êtes tous ${totalPeople} enchantés !`);

    } else {
        console.log(`Vous êtes toutes ${totalPeople} enchantées !`);

    }

} else {
    if(gender == 'm') {
        console.log(`Vous êtes ${totalPeople} enchanté !`);

    } else {
        console.log(`Vous êtes ${totalPeople} enchantée !`);

    }

}

//Ternaire (condition) ? si oui : si non
// Ca on fait pas, c'est illisible 
console.log(`Vous êtes ${(totalPeople <= 1) ? '' : (gender === "m") ? 'tous' : 'toutes'} ${totalPeople} enchanté${ (totalPeople > 1 && gender == 'm')? 's' : (totalPeople > 1 && gender == 'f') ? 'es' : (totalPeople <= 1 && gender == 'f' ) ?  'e' : ''  } !`);


let personne = {
    prenom : 'Aude',
    nom : 'Beurive',
    job : 'Formatrice'
}
console.log(personne.job);
//console.log(personne['job']);
console.log(personne);
delete personne.job;
console.log(personne.job);
console.log(personne);

