console.log("Demo dates");
// Création
let today = new Date(); //Vide : date du jour
console.log(today);
let birthdate = new Date('1989-10-16');
console.log(birthdate);
let birthdateAurelien = new Date(1989, 10, 1); 
//! Attention en JS, les mois commencent à  0 (Janvier) jusque 11(Décembre)
console.log(birthdateAurelien);
let chepakelledate = new Date(4163415463); 
//Date de base (1/1/1970) + 4163415463 ms

// Manipulation
console.log(today.getFullYear());
console.log(today.getMonth()); //! Attention mois -1
console.log(today.getDate()); // Jour du mois entre 1 et 31
console.log(today.getDay()); // Jour de la semaine de 0 (Dimanche) à 6 (Samedi)

// today.setDate(65);
// console.log(today);

// today.setDate(today.getDate() + 3)
console.log(today);

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString
//https://reference.codeproject.com/javascript/Reference/Global_Objects/Date/toLocaleDateString
console.log(today.toLocaleDateString()); //Locale machine
console.log(today.toLocaleDateString('en-US')); //Locale précise
console.log(today.toLocaleDateString('fr-BE', { weekday : "long", day : "2-digit", month : "long", year : "numeric"})); //Locale + options


let months = ["Janvier", "Février", "Mars" /*, ...*/];
let days = ["Dimanche", "Lundi", "Mardi" /*, ...*/];
months[new Date(1989, 2, 3).getMonth()]

// Démo Librairie
//https://day.js.org/en/
//Date du jour
// console.log(dayjs());
// //Date custom
// console.log(dayjs('1982-05-06'));
// //Ajouter 3 jours
// console.log(dayjs().add(3, 'day'));

// Méthode à utiliser avec ajout d'un plugin
//console.log(dayjs('2023-08-16 10:12').fromNow());

